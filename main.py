import unittest
from unittest import suite
import configparser

from qumu_tests import login_tests, home_tests


class TestLoaderWithKwargs(unittest.TestLoader):
    """TestLoader.

    A test loader which allows to parse keyword arguments to the
    test case class."""

    def loadTestsFromTestCase(self, testCaseClass, **kwargs):
        """Return a suite of all tests cases contained in testCaseClass."""
        if issubclass(testCaseClass, suite.TestSuite):
            raise TypeError("Test cases should not be derived from "
                            "TestSuite. Maybe you meant to derive from"
                            " TestCase?")
        testCaseNames = self.getTestCaseNames(testCaseClass)
        if not testCaseNames and hasattr(testCaseClass, 'runTest'):
            testCaseNames = ['runTest']

        # Modification here: parse keyword arguments to testCaseClass.
        test_cases = []
        for test_case_name in testCaseNames:
            test_cases.append(testCaseClass(test_case_name, **kwargs))
        loaded_suite = self.suiteClass(test_cases)

        return loaded_suite


def main():
    """Main engine for the automation."""
    config = configparser.ConfigParser()
    config.read('runbook.ini')
    modulesToRun = config['run']['modules']
    browsers = config['run']['browsers']
    moduleList = modulesToRun.split(",")
    browserList = browsers.split(",")
    # testFolder = config['suiteConfig']['testFolder']
    # print(dir(BaseTestCase))
    print("-"*79)
    print("Runbook details:")
    print("Modules to test: {0}".format(moduleList))
    print("Browsers: {0}".format(browserList))
    if len(moduleList) > 0 and len(browserList) > 0:
        print("-"*79)
        suites = unittest.TestSuite()
        loader = TestLoaderWithKwargs()
        for browser in browserList:
            for module in moduleList:
                print("Adding module: " + module)
                suites.addTests(loader.loadTestsFromTestCase(eval(module),
                                                             browser=browser))
        unittest.TextTestRunner(verbosity=2).run(suites)
    else:
        print("Missing either modules or browsers in runbook")

if __name__ == '__main__':
    main()
