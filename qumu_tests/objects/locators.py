from selenium.webdriver.common.by import By

# ************** Page Objects *************


class TopMenu():
    """Locators for topmenu items."""

    USER_DROPDOWN = (By.ID, "user-name")


class LoginPageLocators():
    """Login Page Locators."""

    LOGIN_FRAME = (By.CSS_SELECTOR, "iframe")
    SECTION_HEADER = (By.CSS_SELECTOR, "h2.title")
    EMAIL_ID_TEXTBOX = (By.ID, 'user_email')
    PASSWD_TEXTBOX = (By.ID, 'user_password')
    STAY_SIGNED_IN_CHECHBOX = (By.CSS_SELECTOR, '.remember>label')
    GO_BUTTON = (By.CSS_SELECTOR, 'input[type="submit"][value="Sign in"]')
    CANCEL_LINK = (By.CLASS_NAME, "cancel")
    SECURE_LABEL = (By.CLASS_NAME, 'secure-disclaimer')
    FORGET_PASSWORD_LINK = (By.CSS_SELECTOR, '#zendesk-js-forgot-password>a')
    SIGNUP_LABEL = (By.CSS_SELECTOR, '.sign_up > .question-text')
    SIGNUP_LINK = (By.CSS_SELECTOR, '.sign_up.question>a')
    GET_PASSWORD_LABEL = (By.CSS_SELECTOR, '.get_password > .question-text')
    GET_PASSWORD_LINK = (By.CSS_SELECTOR, '.get_password.question>a')
    GET_FOOTNOTE_TEXT = (By.CSS_SELECTOR, '.footnotes>p ~ p')
