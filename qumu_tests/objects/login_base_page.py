"""Locators.

This file contains all the locators for the login Page.

"""

from selenium.webdriver.common.by import By
from framework.BasePage import BasePage
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from qumu_tests.objects.locators import LoginPageLocators as LPL
from qumu_tests.objects.locators import  TopMenu


class LoginBasePage(BasePage):
    """Locators for Login Base Page.

    It contains most of the locators for login page.

    """

    _url = "/"

    # ************** Page Operations *************

    def __init__(self, driver, base_url='https://qumunity.qumu.com'):
        """Initialization."""
        self._url = base_url + self._url
        self.driver = driver
        self.timeout = 30

    def load(self):
        self.open(self._url)
        self.driver.switch_to.frame(self.driver.find_element(
            *LPL.LOGIN_FRAME))


    def get_section_header(self):
        # self.driver.switch_to.frame(self.driver.find_element(
        #     *LPL.LOGIN_FRAME))
        val = self.driver.find_element(*LPL.SECTION_HEADER).text
        return val

    def login(self, username, password):
        # self.driver.switch_to.frame(self.driver.find_element(
        #     *LPL.LOGIN_FRAME))
        self.send_text(LPL.EMAIL_ID_TEXTBOX, username)
        self.send_text(LPL.PASSWD_TEXTBOX, password)
        self.click_element(*LPL.GO_BUTTON)
        self.wait_for_element(*TopMenu.USER_DROPDOWN)

    def click_cancel(self):
        """clicks on cancel button"""
        self.click_element(*LPL.CANCEL_LINK)
